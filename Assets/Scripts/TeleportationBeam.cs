﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class TeleportationBeam : MonoBehaviour {

	public Transform player;
	public Transform reticle;
	public float range;

	public LineRenderer laser;

	private Vector3 teleportNormal;

	SteamVR_Controller.Device device;
	SteamVR_TrackedObject trackedObj;

	// Use this for initialization
	void Awake () {
		trackedObj = GetComponent<SteamVR_TrackedObject>();
	}

	// Update is called once per frame
	void Update () {
		device = SteamVR_Controller.Input((int)trackedObj.index);

		RaycastHit hit;
		bool foundHit;

		if (device.GetPress(SteamVR_Controller.ButtonMask.Trigger))
		{
			foundHit = Physics.Raycast(transform.position, transform.forward, out hit, range);

			if (foundHit)
			{
				// for laser line rendering
				Vector3[] points = new Vector3[2];
				points[0] = transform.position;
				points[1] = hit.point;
				reticle.gameObject.SetActive(true);
				laser.SetPositions(points);
				reticle.position = hit.point;
				teleportNormal = hit.normal;
			}
		}

		if (device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
		{
			reticle.gameObject.SetActive(false);
		}

		if (device.GetPressDown(SteamVR_Controller.ButtonMask.Grip) && reticle.gameObject.activeSelf)
		{
				player.position = reticle.position;
				player.up = teleportNormal;
		}

	}
}
